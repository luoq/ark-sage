/**
 * Classes that are used to represent observable features of documents.
 * 
 * @author Yanchuan Sim
 * @version 0.1
 */
package edu.cmu.cs.ark.sage.features;

