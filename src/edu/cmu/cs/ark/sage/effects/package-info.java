/**
 * A collection of effects that can be used to represent the different sparse SAGE effects (eta vectors) generating the observed text/features. You can extend the base {@link edu.cmu.cs.ark.sage.effects.Effect} to suit your needs.
 * 
 * @author Yanchuan Sim
 * @version 0.1
 */
package edu.cmu.cs.ark.sage.effects;

