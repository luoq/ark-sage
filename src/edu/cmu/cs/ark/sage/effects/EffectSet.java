package edu.cmu.cs.ark.sage.effects;

import java.util.HashSet;

/**
 * This class stores a set of {@link Effect}. It is essentially a {@link HashSet} of {@link Effect}s.
 * 
 * @author Yanchuan Sim
 * @version 0.1
 */
@SuppressWarnings("serial")
public class EffectSet extends HashSet<Effect>
{

}
